import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { DocumentManagerViews } from '../../app.component';
import { Folder } from '../../file-navigator/file-navigator.component';

export type Views = string | 'editor' | 'navigator';

@Injectable({
  providedIn: 'root'
})
export class AppService {
  private zeroFolder: Folder = {
    id: '0',
    label: '0',
    createdOn: '0',
    updatedOn: '0',
    parentFolder: '0',
    itemsCount: 0,
    contents: [],
  };
  private zeroView: DocumentManagerViews = {
    DocumentEditor: false,
    FileWalker: false
  };
  private currentViewSource_: BehaviorSubject<DocumentManagerViews> = new BehaviorSubject<DocumentManagerViews>(this.zeroView);
  private userFolderSource_: BehaviorSubject<Folder> = new BehaviorSubject<Folder>(this.zeroFolder);
  constructor() { }

  public userFolderChanged$(): Observable<Folder> {
    return this.userFolderSource_.asObservable();
  }

  public setUserFolder(folder: Folder): void {
    this.userFolderSource_.next(folder);
  }

  public getUserFolder(): Folder {
    return this.userFolderSource_.value;
  }
  public listenForViewChange(): Observable<DocumentManagerViews> {
    return this.currentViewSource_.asObservable();
  }

  public changeView(view: Views) {
    switch (view) {
      case'editor':
        this.currentViewSource_.next({
          ...this.zeroView,
          DocumentEditor: true
        } as DocumentManagerViews);
        break;
      case'navigator':
        this.currentViewSource_.next({
          ...this.zeroView,
          FileWalker: true
        });
        break;
    }
  }
}

export const FileNavigator = (): Views  => 'navigator';
export const DocumentEditorView = (): Views => 'editor';
