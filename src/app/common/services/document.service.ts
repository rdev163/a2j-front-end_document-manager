import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { DocumentData, SectionModel } from '../../document-manager/document-viewer/document-viewer.component';
import { DOCUMENT_FIXTURE } from '../../../models/fake-data.model';
import { restoreSelection } from '../utils/selectable-edit.utils';

@Injectable({
  providedIn: 'root'
})
export class DocumentService {
  private document_source: BehaviorSubject<DocumentData> = new BehaviorSubject<DocumentData>(DOCUMENT_FIXTURE);
  private sectionModels_source: BehaviorSubject<SectionModel[]> = new BehaviorSubject<SectionModel[]>([]);
  private showToolbar_source: BehaviorSubject<boolean> = new BehaviorSubject(false);
  public savedSelection: any = undefined;

  constructor() { }

  public executeCommand(command: string): void {
    if (restoreSelection(this.savedSelection)) {
      document.execCommand(command, false, null);
    }
  }

  public listenToolbarVisible(): Observable<boolean> {
    return this.showToolbar_source.asObservable();
  }

  public toggleEditorToolBar(show?: boolean) {
    if (typeof show === 'boolean') {
      this.showToolbar_source.next(show);
    console.log(this.showToolbar_source.value);
    } else  {
      this.showToolbar_source.next(!this.showToolbar_source.value);
    }
  }

  public sectionModels(): SectionModel[] {
    return this.sectionModels_source.value;
  }

  public onChangedSectionModels$(): Observable<SectionModel[]> {
    return this.sectionModels_source.asObservable();
  }

  public updateSectionModels(sectionModels: SectionModel[]) {
    this.sectionModels_source.next(sectionModels);
  }

  public updateCurrentDocument(doc: DocumentData): void {
    this.document_source.next(doc);
  }

  public currentDocument$(): Observable<DocumentData> {
    return this.document_source.asObservable();
  }
}
