import { Component, OnInit } from '@angular/core';
import {DocumentService} from '../../common/services/document.service';

@Component({
  selector: 'app-editor-toolbar',
  templateUrl: './editor-toolbar.component.html',
  styleUrls: ['./editor-toolbar.style.scss']
})
export class EditorToolbarComponent implements OnInit {
  public toolButtons = [
    {
      iconName: 'format_bold',
      id: 'FormatBold',
      active: false,
      cmd: 'bold'
    },

    {
      iconName: 'format_italic',
      id: 'FormatItalic',
      active: false,
      cmd: 'italic'
    },

    {
      iconName: 'format_underline',
      id: 'FormatUnderline',
      active: false,
      cmd: 'underline'
    },

    {
      iconName: 'format_strikethrough',
      id: 'FormatStrikethrough',
      active: false,
      cmd: 'strikeThrough'
    },

    {
      iconName: 'format_size',
      id: 'FormatSize',
      active: false,
      cmd: ''
    },

    {
      iconName: 'font_download',
      id: 'FontFamily',
      active: false,
      cmd: ''
    },

    {
      iconName: 'format_align_center',
      id: 'FormatAlign_center',
      active: false,
      cmd: 'justifyCenter'
    },

    {
      iconName: 'format_align_left',
      id: 'FormatAlign_left',
      active: false,
      cmd: 'justifyLeft'
    },

    {
      iconName: 'format_align_right',
      id: 'FormatAlign_right',
      active: false,
      cmd: 'justifyRight'
    },

    {
      iconName: 'format_clear',
      id: 'FormatClear',
      active: false,
      cmd: 'removeFormat'
    },
    {
      iconName: 'undo',
      id: 'UndoEdit',
      active: false,
      cmd: 'undo'
    },
    {
      iconName: 'redo',
      id: 'RedoEdit',
      active: false,
      cmd: 'redo'
    }
  ];
  constructor(
    public _documents: DocumentService) { }

  ngOnInit() {
  }

  triggerCommand(command: string): void {
    this._documents.executeCommand(command);
  }
}
