import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { DocumentService } from '../../common/services/document.service';
import { saveSelection } from '../../common/utils/selectable-edit.utils';
import { FormBuilder } from '@angular/forms';
import * as moment from 'moment';

export interface DocumentViewerConfigs {
  documentId: string;
  pageHeight: number;
}

export interface FooterValues {
  left: string;
  right: string;
}

export interface ByMatters {
  template: string;
}

export interface SplitSection {
  parentSection: Section;
  section: Section;
}

export interface DocumentData {
  id: string;
  name: string;
  labels: string[];
  representative: string;
  represented: string;
  relations: string[];
  matter_id: string;
  sections: Section[];
  updatedOn?: Date | number | string;
  createdOn: Date | number | string;
  locked: boolean;
}

export interface SectionModel extends Section {
  editable: boolean;
}

export interface Section {
  position?: number;
  text?: string;
  html?: string;
  styles?: string[];
}

@Component({
  selector: 'app-document-viewer',
  templateUrl: './document-viewer.component.html',
  styleUrls: ['./document-viewer.style.scss']
})
export class DocumentViewerComponent implements OnInit, OnDestroy {
  public tabState = {
    Document: {
      active: false
    },
    Details: {
      active: true
    }
  };
  private subscriptions: Subscription;
  public detailsForm = this._fb.group({
    locked: this._fb.control(false),
    name: this._fb.control(''),
    representative: this._fb.control('No Representative'),
    represented: this._fb.control('None'),
    updatedOn: this._fb.control(''),
  });

  representative = '';
  representatives = ['Alanis Morissette', 'Boba Fett', 'Darth Vader', 'Yoda'];
  profiles = ['Billy Joel', 'Cher', 'David Bowie', 'Meatloaf'];

  constructor(
    public _documents: DocumentService,
    private _fb: FormBuilder) { }

  public ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }

  public ngOnInit(): void {
    this.subscriptions = this._documents.currentDocument$()
      .subscribe((document: DocumentData) => {
        this._documents.updateSectionModels(this.initializeSectionModels(document));
        this.detailsForm.controls['updatedOn'].patchValue(moment(document.updatedOn).fromNow());
        this.detailsForm.controls['name'].patchValue(document.name);
        this.detailsForm.controls['locked'].patchValue(document.locked);
        this.detailsForm.controls['represented'].patchValue(document.represented);
        this.detailsForm.controls['representative'].patchValue(document.representative);
      });
  }

  public onBlur(): void {
    this._documents.savedSelection = saveSelection();
  }

  public initializeSectionModels(doc: DocumentData): SectionModel[] {
    return doc.sections
      .map(section => {
        return {
          ...section,
          editable: false
        };
      });
  }

  public handleTabTap(id): void {
    for (const tab of Object.keys(this.tabState)) {
      this.tabState[tab].active = false;
    }
    this.tabState[id].active = true;
  }

}
