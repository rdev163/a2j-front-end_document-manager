import { Component, OnDestroy, OnInit} from '@angular/core';
import { DocumentData } from './document-viewer/document-viewer.component';
import { DOCUMENT_FIXTURE } from '../../models/fake-data.model';
import { Subscription } from 'rxjs';
import { MessageService } from '../common/services/message.service';
import { DocumentService } from '../common/services/document.service';

@Component({
  selector: 'app-document-manager',
  templateUrl: './document-manager.component.html',
  styleUrls: ['./document-manager.style.scss']
})
export class DocumentManagerComponent implements OnInit, OnDestroy {
  public currentDocumentData: DocumentData;
  private subscriptions: Subscription;
  public editorVisible = false;

  constructor(
    private messages: MessageService,
    private _documents: DocumentService) {
  }

  public ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }

  public ngOnInit(): void {
    this.currentDocumentData = DOCUMENT_FIXTURE;
    this._documents.listenToolbarVisible()
      .subscribe(show => {
        this.editorVisible = show;
      });
    this._documents.updateCurrentDocument(this.currentDocumentData);
  }
}
