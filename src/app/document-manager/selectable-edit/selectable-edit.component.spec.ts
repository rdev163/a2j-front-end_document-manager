import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectableEditComponent } from './selectable-edit.component';

describe('EditSectionComponent', () => {
  let component: SelectableEditComponent;
  let fixture: ComponentFixture<SelectableEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectableEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectableEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
