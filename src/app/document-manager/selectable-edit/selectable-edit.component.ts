import {
  Component, EventEmitter, forwardRef,
  Input, OnDestroy, OnInit, Output, Renderer2, ViewChild
} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { CommandExecutorService } from '../../common/services/command-executor.service';
import { SectionModel } from '../document-viewer/document-viewer.component';
import {restoreSelection, saveSelection} from '../../common/utils/selectable-edit.utils';
import { Subscription } from 'rxjs';
import { DocumentService } from '../../common/services/document.service';

@Component({
  selector: 'app-selectable-edit',
  templateUrl: './selectable-edit.component.html',
  styleUrls: ['./selectable-edit.style.scss'],
  // providers: [{
  //   provide: NG_VALUE_ACCESSOR,
  //   useExisting: forwardRef(() => SelectableEditComponent),
  //   multi: true
  // }]
})
export class SelectableEditComponent implements OnInit, ControlValueAccessor {
  @Input() editable = false;
  @Input() sectionModel: SectionModel;
  @Output() blur: EventEmitter<SectionModel> = new EventEmitter<SectionModel>();
  @Output() focus: EventEmitter<string> = new EventEmitter<string>();
  @ViewChild('editableTextArea') textArea: any;
  private onChange: (value: string) => void;
  private onTouched: () => void;

  constructor(
    public _documents: DocumentService,
    public _commandExec: CommandExecutorService,
    public _render: Renderer2) { }

  ngOnInit() {
    this.writeValue(this.sectionModel.html);
  }

  onEditableSectionBlur(): void {
    this._documents.savedSelection = saveSelection();
    if (typeof this.onTouched === 'function') {
      this.onTouched();
    }
    this.blur.emit(this.sectionModel);
  }

  onSelectableFocus(): void {
    this.onTextAreaFocus();
  }

  onTextAreaFocus(): void {
    restoreSelection(this._commandExec.savedSelection);
    // this.focus.emit('focus');
  }

  onDblClick() {
    this._documents.toggleEditorToolBar(true);
    this.onSelectableFocus();
    this.sectionModel.editable = true;
  }

  executeCommand(commandName: string): void {
    this._commandExec.execute(commandName);
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  writeValue(value: any): void {
    if (value === null || value === undefined || value === '' || value === '<br>') {
      value = null;
    }
    this.refreshView(value);
  }

  onContentChange(html: string): void {
    if (typeof this.onChange === 'function') {
      this.onChange(html);
    }
  }

  refreshView(value: any): void {
    const normalizedValue = value === null ? '' : value;
    this._render.setProperty(this.textArea.nativeElement, 'innerHTML', normalizedValue);
  }

  focusOnTextArea() {
    this._documents.toggleEditorToolBar(true);
    this.textArea.nativeElement.focus();
  }
}
