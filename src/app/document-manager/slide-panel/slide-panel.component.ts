import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { Section } from '../document-viewer/document-viewer.component';
import { Subscription } from 'rxjs';
import { DocumentService } from '../../common/services/document.service';

@Component({
  selector: 'app-slide-panel',
  templateUrl: './slide-panel.component.html',
  styleUrls: ['./slide-panel.style.scss']
})
export class SlidePanelComponent implements OnInit, OnDestroy {
  @Output() tabTap: EventEmitter<string> = new EventEmitter<string>();
  @Input() tabState: {
    [tab: string]: {
      active: boolean;
    }
  };
  public defaultSection: Section = {
    position: 0,
    text: '',
    html: '',
    styles: []
  };
  public sectionModel: Section = this.defaultSection;
  private subscriptions: Subscription;

  constructor(
    private _documents: DocumentService) {}

  public ngOnDestroy(): void {
      this.subscriptions.unsubscribe();
  }

  public ngOnInit(): void {
  }

  public editorBlurHandler(): void {
  }

  public handleTabTap(event: {target: {textContent: string}}) {
    console.log(event.target.textContent);
    this.tabTap.emit(event.target.textContent);
  }

}
