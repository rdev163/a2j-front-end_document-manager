import {Component, OnInit} from '@angular/core';
import {AppService, FileNavigator} from './common/services/app.service';

export interface DocumentManagerViews {
  DocumentEditor: boolean;
  FileWalker: boolean;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.style.scss']
})
export class AppComponent implements OnInit {
  constructor(public _app: AppService) {}

  ngOnInit(): void {
    this._app.changeView(FileNavigator());
  }
}

