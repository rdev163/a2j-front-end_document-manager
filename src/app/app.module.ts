import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {
  MatAutocompleteModule,
  MatButtonModule, MatButtonToggleModule, MatCheckboxModule, MatFormFieldModule, MatGridListModule,
  MatIconModule, MatInputModule, MatMenuModule, MatOptionModule, MatPaginatorModule,
  MatRippleModule, MatSelectModule, MatSortModule, MatTableModule, MatToolbarModule
} from '@angular/material';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DocumentViewerComponent } from './document-manager/document-viewer/document-viewer.component';
import { CommandExecutorService } from './common/services/command-executor.service';
import { MessageService } from './common/services/message.service';
import { DocumentManagerComponent } from './document-manager/document-manager.component';
import { EditorToolbarComponent } from './document-manager/editor-toolbar/editor-toolbar.component';
import { SelectableEditComponent } from './document-manager/selectable-edit/selectable-edit.component';
import { ListItemComponent } from './file-navigator/list-item/list-item.component';
import { ListHeaderComponent } from './file-navigator/list-header/list-header.component';
import { FileNavigatorComponent } from './file-navigator/file-navigator.component';

@NgModule({
  declarations: [
    AppComponent,
    DocumentViewerComponent,
    DocumentManagerComponent,
    SelectableEditComponent,
    EditorToolbarComponent,
    ListItemComponent,
    ListHeaderComponent,
    FileNavigatorComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    MatToolbarModule,
    MatIconModule,
    MatRippleModule,
    MatButtonModule,
    MatMenuModule,
    MatButtonToggleModule,
    MatFormFieldModule,
    MatInputModule,
    MatOptionModule,
    MatSelectModule,
    MatCheckboxModule,
    MatGridListModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatAutocompleteModule
  ],
  providers: [CommandExecutorService, MessageService],
  bootstrap: [AppComponent]
})
export class AppModule { }
