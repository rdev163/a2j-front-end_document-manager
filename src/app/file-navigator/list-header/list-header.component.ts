import {Component, EventEmitter, Input, OnChanges, OnInit, Output} from '@angular/core';
import {PaginatedStateUpdate, PaginationState} from '../file-navigator.component';

export interface HistoryLink {
  id: string;
  label: string;
  index: number;
}

@Component({
  selector: 'app-list-header',
  templateUrl: './list-header.component.html',
  styleUrls: ['./list-header.style.scss']
})
export class ListHeaderComponent implements OnInit, OnChanges {
  @Input() historyChain: HistoryLink[];
  @Input() paginated: PaginationState;
  @Output() paginatedChange: EventEmitter<PaginatedStateUpdate> = new EventEmitter<PaginatedStateUpdate>();
  @Output() historySelected: EventEmitter<HistoryLink> = new EventEmitter<HistoryLink>();
  @Output() backTapped: EventEmitter<string> = new EventEmitter<string>();
  firstItemCount: number;
  lastItemCount: number;
  title: string;

  constructor() { }

  ngOnInit() {
    this.firstItemCount = this.paginated.pageIndex * this.paginated.pageSize + 1;
    this.lastItemCount = ((this.firstItemCount + this.paginated.pageSize) > this.paginated.listLength)
      ? this.paginated.listLength
      : this.firstItemCount + this.paginated.pageSize;
  }

  ngOnChanges() {
    this.title = this.historyChain[this.historyChain.length - 1].label;
  }

  handlePageSizeSelect(nextSize: number) {
    this.paginatedChange.emit({ pageIndex: this.paginated.pageIndex, pageSize: nextSize});
  }
}
