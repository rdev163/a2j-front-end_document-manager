import { Component, OnDestroy, OnInit } from '@angular/core';
import { BehaviorSubject, Subscription } from 'rxjs';
import { FormBuilder } from '@angular/forms';
import { HistoryLink } from './list-header/list-header.component';
import { USER_FOLDER_FIXTURE } from '../../models/fake-data.model';
import { AppService, DocumentEditorView } from '../common/services/app.service';

export type FileNavigatorType = 'doc' | 'folder';

// File Wrapper?
export interface FileNavigatorItem {
  fnType: FileNavigatorType;
  info: Doc | Folder | any;
}

export interface Doc {
  id: string | number;
  label: string;
  template?: string;
  representative: string;
  represented: string;
  updatedOn: Date | number | string;
  createdOn: Date | number | string;
  parentFolder: string;
  matters: string;
}

export interface Folder {
  id: string | number;
  label: string;
  parentFolder: string | null;
  contents: FileNavigatorItem[];
  itemsCount: number;
  createdOn: Date | number | string;
  updatedOn: Date | number | string;
}

export interface PaginationState {
  previousPageIndex: number;
  pageIndex: number;
  pageSize: number;
  listLength: number;
  totalPages: number;
  sizeOptions: number[];
}

export interface PaginatedStateUpdate {
  pageSize: number;
  pageIndex: number;
}

@Component({
  selector: 'app-file-navigator',
  templateUrl: './file-navigator.component.html',
  styleUrls: ['./file-navigator.style.scss']
})
export class FileNavigatorComponent implements OnInit, OnDestroy {
  listNodeData: FileNavigatorItem[] = [];
  defaults: any = {
    historyChain: [{label: 'Home', id: '0'}],
    paginated:  {
      previousPageIndex: 0,
      pageIndex: 0,
      pageSize: 10,
      listLength: 0,
      totalPages: 0,
      sizeOptions: [5, 10, 20, 50, 100],
    }
  };
  historyChainSource: BehaviorSubject<HistoryLink[]> = new BehaviorSubject<HistoryLink[]>(this.defaults.historyChain);
  itemsSource: BehaviorSubject<FileNavigatorItem[]> = new BehaviorSubject<FileNavigatorItem[]>([]);
  paginated: PaginationState = this.defaults.paginated;
  searchGroup = this._fb.group({
    searchQueryControl: this._fb.control('')
  });
  subscription: Subscription;
  searchResults: any[] = [];

  constructor(
    private _fb: FormBuilder,
    private _app: AppService
  ) { }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  ngOnInit() {
    this.initializeComponent();
    this._app.setUserFolder(USER_FOLDER_FIXTURE);
  }

  handlePaginatedUpdate(update: PaginatedStateUpdate) {
    this.updatePaginated(update);
    this.itemsSource.next(this.paginate());
  }

  handleItemTap(item: FileNavigatorItem): void {
    const {fnType} = item;
    switch (fnType) {
      case'doc':
        // todo init a doc store
        this._app.changeView(DocumentEditorView());
        return;
      case'folder':
        const currentLinks = this.historyChainSource.value;
        const nextLinks = currentLinks.concat({
          label: item.info.label,
          id: item.info.id,
          index: currentLinks.length,
        });
        this.historyChainSource.next(nextLinks);
        this.setCurrentNode(item.info.contents);
    }
  }

  handleHistoryTap(link: HistoryLink): void {
    let nextList: FileNavigatorItem[];
    const truncatedChain = this.historyChainSource.value.slice(0, link.index - 1);
    if (truncatedChain.length > 0) {
      nextList = this._app.getUserFolder().contents
        .filter(item => item.info.id === link.id)[0].info.contents;
      this.historyChainSource.next(truncatedChain);
    } else {
      nextList = this._app.getUserFolder().contents;
      this.historyChainSource.next(this.defaults.historyChain);
    }
    this.setCurrentNode(nextList);
  }

  handleBackTap(): void {
    let nextItems = [];
    const chain = this.historyChainSource.value;
    const nextChain = chain.slice(0, chain.length - 1);
    const current = nextChain[nextChain.length - 1];
    if (current.id === '0') {
      nextItems = this._app.getUserFolder().contents;
    } else {
      nextItems = this._app.getUserFolder().contents
        .filter(item => {
          return item.info.id === current.id;
      })[0].info.contents;
    }
    if (nextItems) {
      this.setCurrentNode(nextItems);
      this.historyChainSource.next(nextChain);
    }
  }

  updatePaginated(update: PaginatedStateUpdate = {pageIndex: 0, pageSize: this.paginated.pageSize}): void {
    const { pageSize, pageIndex } = update;
    this.paginated.pageSize = pageSize;
    this.paginated.listLength = this.listNodeData.length;
    this.paginated.totalPages = Math.ceil(this.listNodeData.length / this.paginated.pageSize);
    this.paginated.previousPageIndex = this.paginated.pageIndex;
    if (this.paginated.pageIndex !== pageIndex && pageIndex >= 0 && pageIndex < this.paginated.totalPages) {
      this.paginated.pageIndex = pageIndex;
    }
  }

  paginate(data: any[] = this.listNodeData, index: number = this.paginated.pageIndex, pageSize: number = this.paginated.pageSize): any[] {
    const start = index * pageSize;
    const end = start + pageSize;
    if (end > data.length) {
      return data.slice(start);
    }
    return data.slice(start, end);
  }

  setCurrentNode(data: FileNavigatorItem[]) {
    this.listNodeData = data;
    this.updatePaginated();
    const initItems = this.paginate();
    this.itemsSource.next(initItems);
  }

  initializeComponent(): void {
    this.subscription = this._app.userFolderChanged$()
      .subscribe(folder => {
        if (!folder) {
          this.setCurrentNode([]);
          return;
        }
        const contents = folder.contents;
        this.setCurrentNode(contents);
      });
  }
}
