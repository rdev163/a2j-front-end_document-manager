import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import { FileNavigatorItem } from '../file-navigator.component';
import * as moment from 'moment';

@Component({
  selector: 'app-list-item',
  templateUrl: './list-item.component.html',
  styleUrls: ['./list-item.style.scss']
})
export class ListItemComponent implements OnInit {
  @Input() data: FileNavigatorItem;
  @Output() itemTapped: EventEmitter<FileNavigatorItem> = new EventEmitter<FileNavigatorItem>();
  iconName = 'smiley';
  date = 'no date';
  constructor() { }

  ngOnInit() {
    this.iconName = this.data.fnType === 'folder' ? 'folder_open' : 'insert_drive_file';
    this.date = moment(this.data.info.createdOn).format("MM/DD/YY");
  }
}
