import { DocumentData } from '../app/document-manager/document-viewer/document-viewer.component';
import { Folder} from '../app/file-navigator/file-navigator.component';

export const USER_FOLDER_FIXTURE: Folder = {
  id: '0',
  label: 'Main',
  createdOn: new Date(Date.now()),
  updatedOn: new Date(Date.now()),
  parentFolder: 'root',
  itemsCount: 2,
  contents: [
    {
      fnType: 'folder',
      info: {
        id: 'Foo232344',
        label: 'Alice\'s Folder',
        createdOn: new Date(Date.now()),
        updatedOn: new Date(Date.now()),
        parentFolder: 'root',
        itemsCount: 2,
        contents: [
          {
            fnType: 'doc',
            info: {
              representative: 'Alice',
              represented: 'Bob',
              template: 'Dissolution With Children',
              matters: '213535645712435',
              id: '123434334545',
              label: 'Bob\'s dissolution',
              createdOn: new Date(Date.now()),
              updatedOn: new Date(Date.now()),
              parentFolder: 'Alice\'s Folder'
            }
          },
          {
            fnType: 'doc',
            info: {
              representative: 'Alice',
              represented: 'Charlie',
              template: 'Dissolution With Children',
              matters: '21355643712435',
              id: '12623412545',
              label: 'Charlie\'s dissolution',
              createdOn: new Date(Date.now()),
              updatedOn: new Date(Date.now()),
              parentFolder: 'Alice\'s Folder'
            }
          }
        ],
      }
    },
    {
      fnType: 'folder',
      info: {
        id: 'Foo2523534',
        label: 'Dian\'s Folder',
        createdOn: new Date(Date.now()),
        updatedOn: new Date(Date.now()),
        parentFolder: 'root',
        itemsCount: 2,
        contents: [
          {
            fnType: 'doc',
            info: {
              representative: 'Dian',
              represented: 'Evan',
              template: 'Dissolution With Children',
              matters: '213535645712435',
              id: '1234345214345',
              label: 'Bob\'s dissolution',
              createdOn: new Date(Date.now()),
              updatedOn: new Date(Date.now()),
              parentFolder: 'Dian\'s Folder'
            }
          },
          {
            fnType: 'doc',
            info: {
              representative: 'Dian',
              represented: 'Francis',
              template: 'Dissolution With Children',
              matters: '21355643712435',
              id: '12654326545',
              label: 'Charlie\'s dissolution',
              createdOn: new Date(Date.now()),
              updatedOn: new Date(Date.now()),
              parentFolder: 'Alice\'s Folder'
            }
          }
        ],
      }
    },
    {
      fnType: 'doc',
      info: {
        representative: 'Alanis Morissette',
        represented: 'Billy Joel',
        template: 'Dissolution With Children',
        matters: '2135356457124335',
        id: 'Baz335345224',
        label: 'We Didn\'t Start the Fire',
        updatedOn: new Date(Date.now()),
        createdOn: new Date(Date.now()),
        parentFolder: 'Main',
      }
    },
    {
      fnType: 'doc',
      info: {
        representative: 'George',
        represented: 'Dave',
        template: 'Dissolution With Children',
        matters: '21355643712435',
        id: '1262dtyrh3412545',
        label: 'Charlie\'s dissolution',
        createdOn: new Date(Date.now()),
        updatedOn: new Date(Date.now()),
        parentFolder: 'Alice\'s Folder'
      }
    },
    {
      fnType: 'doc',
      info: {
        representative: 'Hector',
        represented: 'Genie',
        template: 'Dissolution With Children',
        matters: '21355643712435',
        id: '1262q32r3412545',
        label: 'Billy\'s dissolution',
        createdOn: new Date(Date.now()),
        updatedOn: new Date(Date.now()),
        parentFolder: 'Alice\'s Folder'
      }
    },
    {
      fnType: 'doc',
      info: {
        representative: 'Stephenie',
        represented: 'Tom',
        template: 'Dissolution With Children',
        matters: '21355643712435',
        id: '1262awf3412545',
        label: 'Sam\'s dissolution',
        createdOn: new Date(Date.now()),
        updatedOn: new Date(Date.now()),
        parentFolder: 'Alice\'s Folder'
      }
    }
  ]
};

//tslint:disable
export const DOCUMENT_FIXTURE: DocumentData = {
  id: 'Baz324',
  name: 'We Didn\'t Start the Fire',
  labels: ['bar'],
  representative: 'Alanis Morissette',
  represented: 'Billy Joel',
  relations: ['Cher', 'David Bowie', 'Meatloaf'],
  matter_id: '2135356457124335',
  updatedOn: new Date(1541459149347),
  createdOn: 1541459149347,
  locked: true,
  sections: [
    {
      html: `<div>1. Jurisdiction</div><div><br></div><div>Petitioner has been a resident of the State of Oregon for the six months immediately</div><div>preceding the filing of this Petition. Respondent is a resident of the State of Oregon.</div><div>Irreconcilable differences have caused the irremediable breakdown of the marriage of the parties.</div><div><br></div>`,
      position: 0
    },
    {
      html: `<div>2. OTHER DOMESTIC RELATIONS MATTERS</div><div><br></div><div>There are no other domestic relations matter pending in any jurisdiction involving these</div><div>parties.</div><div>Or</div><div><div>There is another/other matter(s) pending in [jurisdiction]: [name and case number of matter]</div><div>///</div><div><br></div>`,
      position: 1
    },
    {
      html: `<div>3. VITAL STATISTICS</div><div>Wife Husband</div><div><br></div><div>Name:</div><div>Maiden: UTCR 2.130 N/A</div><div>Former: Separately submitted under UTCR 2.130</div><div>Address:</div><div>Date of Birth/Age: Separately submitted under UTCR 2.130</div><div>Social Security Number: Separately submitted under UTCR 2.130</div><div>Driver’s License State/No.: Separately submitted under UTCR 2.130</div><div>Employer Name: Separately submitted under UTCR 2.130</div><div>Employer Address: Separately submitted under UTCR 2.130</div><div>Employer Telephone: Separately submitted under UTCR 2.130</div><div>Date of Marriage:</div><div>Place of Marriage:</div><div>Date of Separation:</div><div><br></div>`,
      position: 2
    },
    {
      html: `<div>4. CHILDREN</div><div><br></div><div>There is/are one child(ren) born at issue of the marriage, namely , age _.; birthdates</div><div>withheld pursuant to UTCR 2.130. Wife is now not pregnant. Husband has _______ non-joint</div><div>child(ren), wife has non-joint child(ren).</div><div><br></div>`,
      position: 3
    },
    {
      html: `<div>5. UCCJEA INFORMATION</div><div><br></div><div>The child(ren) listed above has/have continuously resided in Oregon for the six (6)</div><div>months preceding the filing of this petition and has resided in the following places with the</div><div>following people over the last five (5) years:</div><div>Dates County Address With Whom Child</div><div><br></div><div>The State of Oregon has jurisdiction to award custody to Petitioner. Oregon is the home</div><div>state of the child and both parties reside in Oregon. Petitioner has not participated, as a party,</div><div>witness or in any other capacity, in any other litigation concerning the custody of, or parenting</div><div>time or visitation with, this child in this or any other state. Petitioner knows of no other proceeding that could affect the current proceeding, including proceedings for enforcement and</div><div>proceedings relating to domestic violence, protective orders, termination of parental rights and</div><div>adoptions pending in the courts of this or any other state. Petitioner knows of no person not a</div><div>party to these proceedings who has physical custody of the child or claims of rights of legal or</div><div>physical custody of, or parenting time or visitation, with respect to the child.</div><div><br></div>`,
      position: 4
    },
    {
      html: `<div>6. INFORMATION REQUIRED BY ORS 25.020 AND ORS 107.085</div><div><br></div><div>Wife Husband</div><div>Full Name</div><div>Age</div><div>Address</div><div>Telephone Number</div><div>Wife and Husband’s remaining information as required by ORS 107.085(2) is provided in the</div><div>Segregated Information Sheet as required by UTCR 2.130.</div><div><br></div>`,
      position: 5
    },
    {
      html: `<div>7. CUSTODY AND PARENTING TIME</div><div><br></div><div>Petitioner is a fit and proper person and should be awarded legal and physical custody of</div><div>the parties’ minor child(ren), ________, age __, subject to Respondent’s reasonable parenting</div><div>time.</div><div>OR</div><div>Petitioner and Respondent are each fit and proper persons to be awarded joint legal and</div><div>physical custody of the parties’ minor child(ren), _______, age____. Parenting time should be</div><div>agreed upon by the parties, and if they cannot agree, by the Court.</div><div>8. TEMPORARY CHILD SUPPORT</div><div><br></div><div>Respondent should be required to pay temporary child support in an amount that is in</div><div>conformance with the Oregon Child Support Guidelines during the pendency of this matter</div><div>beginning with the first day of the month following service of this Petition for Dissolution of</div><div>Marriage.</div></div>`,
      position: 6
    }
  ]
};
